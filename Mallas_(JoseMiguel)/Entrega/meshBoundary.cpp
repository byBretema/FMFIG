/*
 * meshBoundary.cpp
 *
 * Written by Jose Miguel Espadero <josemiguel.espadero@urjc.es>
 *
 * This code is written as example for the FMFIG class of the
 * Master Universitario en Informatica Grafica, Juegos y Realidad Virtual.
 * Its purpose is to be didactic and easy to understand, not hard optimized.
 *
 * This file computes the list of internal and external edges of a mesh
 * and writes the boundary (list of external edges). Then create a copy
 * of the mesh over a colorMesh and assign the red color to the vertex
 * in the boundary.
 *
 * //TODO: Fill-in your name and email
 * Name of alumn: Daniel Camba Lamas
 * Email of alumn: <cambalamas@gmail.com>
 * Year: 2017
 *
 */

#define _CRT_NONSTDC_NO_DEPRECATE
#include <ColorMesh3.hpp>
#include <EdgeMesh.hpp>
#include <cmath>
#include <iostream>
#include <time.h> //clock_t, clock, CLOCKS_PER_SEC

#include <queue>
#define Node std::pair<double, int>

int main(int argc, char* argv[])
{

#ifdef _OPENMP
    cout << "Compiled with OpenMP support" << endl;
#endif

    try {
        // Set default input mesh filename
        // std::string filename("mallas/Nefertiti.990kv.ply");
        //std::string filename("mallas/heptoroid.286kv.ply");
        //std::string filename("mallas/maxplanck.45kv.ply");
        std::string filename("mallas/mannequin.off");
        // std::string filename("mallas/mask2.off");
        //std::string filename("mallas/knot-hole.off");
        //std::string filename("mallas/16Triangles.off");

        if (argc > 1)
            filename = std::string(argv[1]);

        ///////////////////////////////////////////////////////////////////////

        //Read a mesh and write a mesh
        EdgeMesh mesh1;
        ColorMesh colorMesh1;
        cout << "Loading file " << filename << endl;
        mesh1.readFile(filename, false);
        colorMesh1.readFile(filename, false);

        cout << "Num vertex: " << mesh1.numVertex() << " Num triangles: " << mesh1.numTriangles()
             << " Unreferenced vertex: " << mesh1.checkUnreferencedVertex() << endl;

        //Init time measures used for profiling
        clock_t clock0 = clock();

        ///////////////////////////////////////////////////////////////////////

        //Compute the list of edges
        //TODO 2.1:
        //Implement the body of the EdgeMesh::updateEdgeLists() method in the file EdgeMesh.hpp.
        mesh1.updateEdgeLists();
        //END TODO 2.1:
        cout << "Done updateEdgeLists() " << double(clock() - clock0) / CLOCKS_PER_SEC << " seconds" << endl;
        cout << mesh1.externalEdges.size() << " boundary edges" << endl;
        cout << mesh1.internalEdges.size() << " internal edges" << endl;
        //Write external boundary and compute boundary length
        double boundaryLength = 0.0;
        cout << "Edges in the boundary:" << endl;
        for (const auto e : mesh1.externalEdges) {
            cout << "[" << e.a << "->" << e.b << "] ";
            boundaryLength += mesh1.length(e);
        }
        cout << endl;
        cout << "Boundary length: " << boundaryLength << endl;

        //Vector to store the distance from a vertex to the nearest vertex in the boundary
        size_t nv = mesh1.coordinates.size();
        std::vector<double> boundDist(nv, DBL_MAX);
        //Index of the vertex with max distance to boundary
        int deepestVertex = -1;
        double maxDist = 0.0;

        //TODO 2.2:
        //Compute the distance from each vertex to the nearest vertex in the boundary (euclidean distance measured along edges)
        //and store it in the boundDist vector.
        //Store in deepestVertex the index of the vertex with the maximum distance to boundary

        // Neighbours...
        std::vector<std::vector<int>> NN;
        mesh1.computeNeighbours(NN);

        // 2. Dijkstra...
        // Constructor way init...
        std::vector<bool> viewed(nv, false);
        std::priority_queue<Node, std::vector<Node>, std::greater<Node>> pq; // Min_Heap...
        // Boundary on the PQ.
        for (const auto& E : mesh1.externalEdges) {
            boundDist[E.a] = 0.0;
            pq.push(Node(boundDist[E.a], E.a));
        }
        // Compute each.
        while (!pq.empty()) {
            auto CurV = pq.top();
            pq.pop();
            viewed[CurV.second] = true;
            // Iter neighbours of curr boundary vertex.
            for (size_t i = 0; i < NN[CurV.second].size(); i++) {
                int CurN = NN[CurV.second][i];
                // double newDist = boundDist[CurV.second] + DD[CurV.second][CurN];
                double newDist = boundDist[CurV.second] + mesh1.distance(CurV.second, CurN);
                // Ignore viewed and eval improvement rate.
                if (!viewed[CurN] && (boundDist[CurN] > newDist)) {
                    // Update distance.
                    boundDist[CurN] = newDist;
                    pq.push(Node(boundDist[CurN], CurN));
                    // Update DeepestVertex.
                    if (newDist > boundDist[deepestVertex]) {
                        deepestVertex = CurN;
                        maxDist = newDist;
                    }
                }
            }
        }

        //END TODO 2.2
        cout << "Done boundDist() " << double(clock() - clock0) / CLOCKS_PER_SEC << " seconds" << endl;

        //Dump distances to boundary (for small meshes)
        // if (boundDist.size() < 40) {
        //     cout << "Distances to boundary: " << endl;
        //     for (size_t i = 0; i < boundDist.size(); i++)
        //         cout << "vertex: " << i << " : " << boundDist[i] << endl;
        //     cout << endl;
        // }

        //Dump the index and distance for the deepestVertex
        cout << "maxDistance: " << boundDist[deepestVertex] << " at vertex: " << deepestVertex << endl;

        /* Dump path from deepestVertex to nearest boundary vertex
        cout << "Path to boundary" << endl;
        int i = deepestVertex;
        while (i != -1)
        {
            cout << i << " -> ";
            i = parent[i];
        }
        cout << endl;
        //Dump path */

        //TODO 2.3:
        //Create a colorMesh where the color of each vertex shows its distance to boundary
        //To create colors, use the function setTemperature(1.0-boundDist[i]/maxDistance)

        //Iterate over the vertex of the mesh and change the color
        //of each vertex using the distance to a given point.
        for (size_t i = 0; i < colorMesh1.numVertex(); i++) {
            float dist = float(1.f - (boundDist[i] / maxDist));
            colorMesh1.colors[i].setTemperature(dist, 0.f, 1.f);
        }
        //END TODO 2.3
        cout << "Done colorize by distance to boundary " << double(clock() - clock0) / CLOCKS_PER_SEC << " seconds" << endl;

        //Save to a file named "output_boundary.obj"
        std::string outputFilename = "output_boundary.obj";
        cout << "Saving output to " << outputFilename << endl;
        colorMesh1.writeFileOBJ(outputFilename);

//Visualize the file with an external viewer
#ifdef WIN32
        string viewcmd = "meshlab.exe";
#else
        string viewcmd = "meshlab >/dev/null 2>&1 ";
#endif
        string cmd = viewcmd + " " + outputFilename;
        cout << "Executing external command: " << cmd << endl;
        return system(cmd.c_str());
    }

    catch (const string& str) {
        std::cerr << "EXCEPTION: " << str << std::endl;
    } catch (const char* str) {
        std::cerr << "EXCEPTION: " << str << std::endl;
    } catch (std::exception& e) {
        std::cerr << "EXCEPTION: " << e.what() << std::endl;
    } catch (...) {
        std::cerr << "EXCEPTION (unknow)" << std::endl;
    }

#ifdef WIN32
    cout << "Press Return to end the program" << endl;
    cin.get();
#else
#endif

    return 0;
}
