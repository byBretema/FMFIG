/*
 * EdgeMesh.hpp
 *
 * Written by Jose Miguel Espadero <josemiguel.espadero@urjc.es>
 *
 * This code is written as example for the FMFIG class of the
 * Master Universitario en Informatica Grafica, Juegos y Realidad Virtual.
 * Its purpose is to be didactic and easy to understand, not hard optimized.
 *
 * This file extends the SimpleMesh class to add two fields to contain the
 * list of edges in the boundary and the list of edges internal to the mesh.
 *
 *
 * //TODO: Fill-in your name and email
 * Name of alumn: Daniel Camba Lamas
 * Email of alumn: <cambalamas@gmail.com>
 * Year: 2017
 *
 */

#ifndef _EDGEMESH3D_
#define _EDGEMESH3D_

#include <SimpleMesh3.hpp>

#include <cfloat>
#include <list>

#include <unordered_map>

///Simple class to store an edge as 2 indexes to an external coordinates container
class SimpleEdge {
public:
    int a;
    int b;

    SimpleEdge()
    {
        a = -1;
        b = -1;
    }

    SimpleEdge(const int _a, const int _b)
    {
        a = _a;
        b = _b;
    }

    /// Set the values of the indexes
    void set(const int _a, const int _b)
    {
        a = _a;
        b = _b;
    }

    /// Look if the edge contais a index
    inline bool contains(const int index) const
    {
        return (a == index || b == index);
    }

    /// Build the reversed version of the edge
    inline SimpleEdge reversed() const
    {
        return SimpleEdge(b, a);
    }

    /// Change the orientation of the edge
    inline void reverseOrientation()
    {
        std::swap(a, b);
    }

    /// Check if two edges are equal
    inline bool operator==(const SimpleEdge& e) const
    {
        return a == e.a && b == e.b;
    }

    /// Check if two edges are different
    inline bool operator!=(const SimpleEdge& e) const
    {
        return a != e.a || b != e.b;
    }

    /// Lexicographically less operator. Usefull to declare sorted containers of edges
    inline bool operator<(const SimpleEdge& e) const
    {
        return (a < e.a) || ((a == e.a) && (b < e.b));
    }

}; //class SimpleEdge

///Extension of SimpleMesh class to add two SimpleEdge containers
class EdgeMesh : public SimpleMesh {
public:
    ///List of external edges (those on the boundary of the mesh)
    std::vector<SimpleEdge> externalEdges;

    ///List of internal edges (those separating two triangles of the mesh)
    std::vector<SimpleEdge> internalEdges;

    ///Compute the length of an edge
    inline double length(const SimpleEdge& e) const
    {
        return coordinates[e.a].distance(coordinates[e.b]);
    }

    // ADD TO ACCEPT "PAIRS" ON THE MAP
    inline size_t key(int i, int j) { return (size_t)i << 32 | (unsigned int)j; }

    /// Update the contents of externalEdges and internalEdges
    void updateEdgeLists(void);

}; // EdgeMesh

/// Update the contents of externalEdges and internalEdges
void EdgeMesh::updateEdgeLists()
{
    //TODO 2.1: Implement the body of the EdgeMesh::updateEdgeLists() method
    std::unordered_map<size_t, std::pair<bool, SimpleEdge>> E;

    // 1. LAMBDA: Checks on a given edge.
    auto check = [&](int ini, int end) {
        auto CK = this->key(ini, end);
        auto RK = this->key(end, ini);
        auto CurrEdge = SimpleEdge(ini, end);
        /*
         * Check "E.count(RK) > 0", because ".at()" throws an "out_of_range" exception.
         * And "[] operator" generate junk on IF sentences.
         */
        if (E.count(RK) > 0 && E.at(RK).first) {
            E.erase(RK);
            internalEdges.push_back(CurrEdge);
        } else {
            E[CK] = std::make_pair(true, CurrEdge);
        }
    };
    // 2. Iter the mesh triangles.
    for (const SimpleTriangle& t : this->triangles) {
        check(t.a, t.b);
        check(t.b, t.c);
        check(t.c, t.a);
    }

    // 3. Simplified map.
    std::unordered_map<int, SimpleEdge> sE;
    for (const auto& pair : E) {
        sE[pair.second.second.a] = pair.second.second;
    }

    // 4. Catch the lower XYZ vertex.
    int nextV;
    double auxX = DBL_MAX;
    double auxY = DBL_MAX;
    double auxZ = DBL_MAX;
    for (const auto& e : sE) {
        auto vertIndex = e.first;
        auto vert = this->coordinates[vertIndex];
        if (vert.X < auxX) { // Usually prune here.
            nextV = vertIndex;
            auxX = vert.X;
        } else if (vert.X == auxX && vert.Y < auxY) {
            nextV = vertIndex;
            auxY = vert.Y;
        } else if (vert.X == auxX && vert.Y == auxY && vert.Z < auxZ) {
            nextV = vertIndex;
            auxZ = vert.Z;
        }
    }

    // 5. Insert with order.
    for (const auto& _ : sE) {
        externalEdges.push_back(sE[nextV]);
        nextV = sE[nextV].b;
    }
    //END TODO 2.1

} //void EdgeMesh::updateEdgeLists()

#endif
