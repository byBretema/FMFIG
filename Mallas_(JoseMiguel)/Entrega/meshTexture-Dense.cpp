/*
 * meshTexture-Dense.cpp
 *
 * Written by Jose Miguel Espadero <josemiguel.espadero@urjc.es>
 *
 * This code is written as example for the FMFIG class of the
 * Master Universitario en Informatica Grafica, Juegos y Realidad Virtual.
 * Its purpose is to be didactic and easy to understand, not hard optimized.
 *
 * //TODO: Fill-in your name and email
 * Name of alumn: Daniel Camba Lamas
 * Email of alumn: <cambalamas@gmail.com>
 * Year: 2017
 *
 */

#define _CRT_NONSTDC_NO_DEPRECATE
#include <iomanip>
#include <iostream>
#include <time.h> //clock_t, clock, CLOCKS_PER_SEC

#include <EdgeMesh.hpp>
#include <TextureMesh.hpp>

//Check if Eigen  (a standar Matrix Library) is included. If not,
//you can get it at http://eigen.tuxfamily.org

// <Eigen/Dense> is the module for dense (traditional) matrix and vector.
// You can get a quick reference for using Eigen dense objects at:
// http://eigen.tuxfamily.org/dox/group__QuickRefPage.html
#include <Eigen/Dense>

/// Write an Eigen Matrix to a matlab file
void exportDenseToMatlab(const Eigen::MatrixXd& m, const std::string& filename, const std::string matrixName = "A")
{
    cout << "Export matrix " << matrixName << " to file: " << filename << std::endl;

    //Open the file as a stream
    ofstream os(filename.c_str());
    if (!os.is_open())
        throw filename + string(": Error creating the file");

    os << "# name: " << matrixName << std::endl
       << "# type: matrix" << std::endl
       << "# rows: " << m.rows() << std::endl
       << "# columns: " << m.cols() << std::endl;
    Eigen::IOFormat OctaveFmt(Eigen::StreamPrecision, Eigen::DontAlignCols, " ", "\n", "", "", "", "\n");
    os << m.format(OctaveFmt);

    os.close();
    std::cout << "To import " << matrixName << " into matlab use the command: load(\"" << filename << "\")" << std::endl;

} //void exportDenseToMatlab (const &MatrixXd m, const std::string &filename)

double cotanOf(vec3 v0, vec3 v1, vec3 v2);
int main(int argc, char* argv[])
{
    //Set dumpMatrix to true for debugging. Writting matrix to file can take some time
    bool dumpMatrix = false;

    try {
        // Set input mesh filename
        //std::string filename("mallas/16Triangles.off");
        std::string filename("mallas/mask2.off");
        // std::string filename("mallas/mannequin2.ply");
        //std::string filename("mallas/maxplanck.45kv.ply");
        //std::string filename("mallas/laurana50k.off");
        if (argc > 1)
            filename = std::string(argv[1]);

        ///////////////////////////////////////////////////////////////////////
        //Step 1. [V]
        //Read an input mesh
        EdgeMesh mesh1;
        cout << "Loading file " << filename << endl;
        mesh1.readFile(filename, false);

        cout << "Num vertex: " << mesh1.numVertex() << " Num triangles: " << mesh1.numTriangles()
             << " Unreferenced vertex: " << mesh1.checkUnreferencedVertex() << endl;

        //Time measure
        clock_t clock0 = clock();

        ///////////////////////////////////////////////////////////////////////
        //Step 2. [V]
        //Compute edge list and show it. This step should work if you correctly
        //finished the meshBoundary exercise.
        mesh1.updateEdgeLists();

        //Count num of internal and external vertex
        size_t numVertex = mesh1.coordinates.size();
        size_t numOfExternalVertex = mesh1.externalEdges.size();
        //size_t numOfInternalVertex = numVertex - numOfExternalVertex;

        //Dump external edges
        cout << numOfExternalVertex << " vertex in external boundary: " << endl;
        if (numOfExternalVertex < 80) {
            auto v = mesh1.externalEdges.begin();
            while (v != mesh1.externalEdges.end()) {
                cout << "[" << v->a << "->" << v->b << "] ";
                v++;
            }
            cout << endl;
        }

        //Build the list of external vertex
        std::vector<size_t> externalVertex;
        externalVertex.reserve(mesh1.externalEdges.size());
        for (auto v = mesh1.externalEdges.begin(); v != mesh1.externalEdges.end(); v++) {
            externalVertex.push_back(v->a);
        }

        //Optimization: Keep a flag to aks if a vertex is external
        std::vector<bool> isExternal(numVertex, false);
        for (size_t i = 0; i < numOfExternalVertex; i++) {
            isExternal[externalVertex[i]] = true;
        }

        cout << "Done compute Boundary. " << double(clock() - clock0) / CLOCKS_PER_SEC << " seconds" << endl;

        ///////////////////////////////////////////////////////////////////////
        //Step 3. [V]
        //Build Laplace matrix (step 1)
        cout << "Computing Laplace matrix (dense):" << endl;

        //We will use a dense matrix. If you want to use sparse matrix
        //you have to check http://eigen.tuxfamily.org/dox/group__TutorialSparse.html
        //and declare a sparse matrix instead.
        Eigen::MatrixXd meshMatrix(numVertex, numVertex);
        // meshMatrix = Eigen::MatrixXd::Zero();

        //TODO 3.1: Build the Laplace matrix
        //
        //      /\         .  Lij = cot(α) + cot(β)
        //     /β \        .
        //    /    \       .
        //   /      \      .
        // vi--------vj    .
        //   \      /      .
        //    \    /       .
        //     \α /        .
        //      \/         .
        //

        for (const auto& t : mesh1.triangles) {
            // Vertex.
            auto A = mesh1.coordinates[t.a];
            auto B = mesh1.coordinates[t.b];
            auto C = mesh1.coordinates[t.c];
            // Cotans.
            auto cotA = cotanOf(A, B, C);
            auto cotB = cotanOf(B, C, A);
            auto cotC = cotanOf(C, B, A);
            // A.
            meshMatrix(t.b, t.c) += cotA;
            meshMatrix(t.c, t.b) += cotA;
            // B.
            meshMatrix(t.a, t.c) += cotB;
            meshMatrix(t.c, t.a) += cotB;
            // C.
            meshMatrix(t.a, t.b) += cotC;
            meshMatrix(t.b, t.a) += cotC;
        }

        //END TODO 3.1

        cout << "Done weights matrix. " << double(clock() - clock0) / CLOCKS_PER_SEC << " seconds" << endl;

        ///////////////////////////////////////////////////////////////////////
        //Step 4. [V]
        //Build Laplace matrix (step 2)
        //Patch the diagonal of the matrix, so every row adds zero
        cout << "Computing Laplacian matrix:" << endl;

        //TODO 3.2: Patch the diagonal of the laplacian matrix
        for (size_t i = 0; i < numVertex; i++)
            meshMatrix(i, i) = -meshMatrix.row(i).sum();
        //END TODO 3.2

        cout << "Done Laplacian matrix (dense). " << double(clock() - clock0) / CLOCKS_PER_SEC << " seconds" << endl;

        if (dumpMatrix)
            exportDenseToMatlab(meshMatrix, "laplacian.mat", "L");

        ///////////////////////////////////////////////////////////////////////
        //Step 5.1 [V]
        //Build system matrix
        cout << "Computing System matrix:" << endl;

        //TODO 3.3: Patch Laplace matrix to generate a valid system of equations
        for (const auto& eV : externalVertex) {
            meshMatrix.row(eV).setZero();
            meshMatrix(eV, eV) = 1; // Diagonal.
        }
        //END TODO 3.3

        cout << "Done System matrix. " << double(clock() - clock0) / CLOCKS_PER_SEC << " seconds" << endl;

        if (dumpMatrix)
            exportDenseToMatlab(meshMatrix, "systemMatrix.mat", "A");

        ///////////////////////////////////////////////////////////////////////
        //Step 5.2 [V]
        //Build UV values for external vertex, using the boundary of a square.
        cout << "Computing contour conditions:" << endl;

        //Note that this is a matrix of numvertex rows and 2 columns
        Eigen::MatrixX2d UV_0;
        UV_0.setZero(numVertex, 2);

        //TODO 3.4: Build a set of valid UV values for external vertex, mapping
        //the vertex to the boundary of a square of side unit.
        double qMod = 1.0 / (externalVertex.size() / 4);
        double aux0 = 0.0;
        double aux1 = 0.0;
        int squareSize = 0;

        for (const auto& eV : externalVertex) {
            UV_0(eV, 0) = aux0;
            UV_0(eV, 1) = aux1;
            switch (squareSize) {
            // 00 -> 10
            case 0:
                aux0 += qMod;
                if (aux0 >= 1.0) {
                    aux0 = 1.0; // Avoid acuracy lost
                    squareSize++;
                }
                break;
            // 10 -> 11
            case 1:
                aux1 += qMod;
                if (aux1 >= 1.0) {
                    aux1 = 1.0; // Avoid acuracy lost
                    squareSize++;
                }
                break;
            // 11 -> 01
            case 2:
                aux0 -= qMod;
                if (aux0 < qMod) {
                    aux0 = 0.0; // Avoid acuracy lost
                    squareSize++;
                }
                break;
            // 01 -> 00
            case 3:
                aux1 -= qMod;
                if (aux1 < qMod) {
                    aux1 = 0.0; // Avoid acuracy lost
                    squareSize++;
                }
                break;
            }
        }

        //END TODO 3.4

        cout << "Done contour conditions. " << double(clock() - clock0) / CLOCKS_PER_SEC << " seconds" << endl;

        if (dumpMatrix)
            exportDenseToMatlab(UV_0, "boundaryUVO.mat", "UV0");

        ///////////////////////////////////////////////////////////////////////
        //Step 6 [X]

        //TODO: Uncomment this section of code if you need to export to matlab/octave
        //Export to matlab/octave (step 6.1 and 6.2)
        if (dumpMatrix) {
            cout << "Exporting to matlab" << endl;
            mesh1.writeFileMatlab("systemMesh.m");
        }
        //END TODO

        ///////////////////////////////////////////////////////////////////////
        //Step 6.3 (without matlab/octave)

        //OK, we will use Eigen to solve the matrix from here. So we don't need
        //wait to matlab. But you WILL have to solve the system in matlab anyway...
        cout << "Solving the system using Eigen (dense):" << endl;

        //Solve Ax = b; where A = meshMatrix and b = UV_0
        //This code is valid only for solving a dense matrix. If you want to use sparse matrix
        //you have to check http://eigen.tuxfamily.org/dox/group__TopicSparseSystems.html
        Eigen::MatrixX2d UV = meshMatrix.lu().solve(UV_0);

        cout << "Done solving the system. " << double(clock() - clock0) / CLOCKS_PER_SEC << " seconds" << endl;

        //Dump the computed solution to the system
        if (dumpMatrix)
            exportDenseToMatlab(UV, "solutionUV.mat", "UV");

        ///////////////////////////////////////////////////////////////////////
        //Step 6.4 (without matlab/octave)
        //This code is the equivalent to execute the exportOBJ.m command in matlab

        //Create a planar mesh (reverse UV mesh)
        SimpleMesh planarMesh;
        //Use UV values as geometrical coordinates
        planarMesh.coordinates.resize(numVertex);
        for (size_t i = 0; i < numVertex; i++) {
            planarMesh.coordinates[i].set(UV(i, 0), UV(i, 1), 0);
        }
        planarMesh.triangles = mesh1.triangles;

        string output_UVMesh1 = "output_UVMesh1.obj";
        cout << "Saving parameterization mesh to " << output_UVMesh1 << endl;
        planarMesh.writeFileOBJ(output_UVMesh1);
        //planarMesh.writeFilePLY(output_UVMesh1);

        //Create a TextureMesh mesh with UV coordinates
        TextureMesh textureMesh;
        textureMesh.coordinates = mesh1.coordinates;
        textureMesh.triangles = mesh1.triangles;

        //Set image filename to be used as texture
        textureMesh.textureFile = "texture.jpg";

        //Set UV as texture-per-vertex coordinates
        textureMesh.UV.resize(numVertex);
        for (size_t i = 0; i < numVertex; i++)
            textureMesh.UV[i].set(UV(i, 0), UV(i, 1));

        //Dump textureMesh to file (.obj or .ply)
        string output_UVMesh2 = "output_UVMesh2.obj";
        cout << "Saving texture mesh to " << output_UVMesh2 << endl;
        textureMesh.writeFileOBJ(output_UVMesh2);
//textureMesh.writeFilePLY(output_UVMesh2);

//Visualize the file with an external viewer
#ifdef WIN32
        string viewcmd = "meshlab.exe";
#else
        string viewcmd = "meshlab >/dev/null 2>&1 ";
#endif
        string cmd = viewcmd + " " + output_UVMesh2;
        cout << "Executing external command: " << cmd << endl;
        return system(cmd.c_str());

    } //try
    catch (const string& str) {
        std::cerr << "EXCEPTION: " << str << std::endl;
    } catch (const char* str) {
        std::cerr << "EXCEPTION: " << str << std::endl;
    } catch (std::exception& e) {
        std::cerr << "EXCEPTION: " << e.what() << std::endl;
    } catch (...) {
        std::cerr << "EXCEPTION (unknow)" << std::endl;
    }

#ifdef WIN32
    cout << "Press Return to end the program" << endl;
    cin.get();
#else
#endif

    return 0;
}

// --- HELPERS

// Compute de cotan of the angle in "v0".
double cotanOf(vec3 v0, vec3 v1, vec3 v2)
{
    //Compute edges
    vec3 e_ab = v1 - v0;
    vec3 e_ac = v2 - v0;
    //Compute sin and cos with cross and dot product
    double sin_A = (e_ab ^ e_ac).module();
    double cos_A = e_ab * e_ac;
    //Compute cotangent of angle in vertex va
    return cos_A / sin_A;
}
