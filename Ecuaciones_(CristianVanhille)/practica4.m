[Yn, Yx, err] = Cauchy();

function [Yn, Yx, err] = Cauchy()
    Yn = CauchyAprox();
    Yx = CauchyExact();
    len = length(Yn);

    err = zeros(1,len);
    for i = 1:1:len
        err(i) = abs(Yn(i)-Yx(i))/Yx(i);
    end
    err = err * 100;
    
    if (length(Yn) == length(Yx))
       figure;
       hold on;
       grid on;
           x = 0:1:len-1;
           plot(x,Yn);
           plot(x,Yx);
           plot(x,err);
       hold off;
    end
    
end
   
function Y = CauchyAprox()
    h = 0.2; %discretización.
    n = ((0 + 1 - 0)/h)+1;

    T = zeros(1,n);
    for i = 2:1:n
       T(i) = T(i-1)+h;
    end

    Y = zeros(1,n);
    Y(1) = 0.5;

    f = @(y,t,h)( y + h*(y-(t^2) +1) );

    for i = 2:1:n
        Y(i) = f(Y(i-1),T(i-1),h);
    end
end

function Y = CauchyExact()
    h = 0.2; %discretización.
    n = ((0 + 1 - 0)/h)+1;

    T = zeros(1,n);
    for i = 2:1:n
       T(i) = T(i-1)+h;
    end

    Y = zeros(1,n);
    f = @(t)(((t+1)^2) - 0.5*exp(t));

    for i = 1:1:n
        Y(i) = f(T(i));
    end
end
