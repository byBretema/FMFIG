
x1 = 1;
x2 = 3;
E = 0.0001;

f = @(x)( x.*( sin( (1 / 2) * x.^2) + exp(-x)) );
f = sym(f); % funcion simplficada.
df = diff(f); % derivada de la funcion.

% BISEC = bisec(f,x1,x2,E);
% display(BISEC);
    
NEWRAP = newRap(f,df,x2,E);
display(NEWRAP);


function retVal = bisec (f,x1,x2,E)

    % calculo de parada
    n = log((x2-x1)/E)/log(2);
    
    for i = 0:1:n

        % punto xM
        xM = (x1+x2)/2;
        retVal = xM;
        fXM = f(xM);
        if fXM == 0
            break
        end

        % punto x1
        fX1 = f(x1);
        if sign(fX1) == sign(fXM)
            x1 = xM;
        end

        % punto x2
        fX2 = f(x2);
        if sign(fX2) == sign(fXM)
           x2 = xM; 
        end

    end % for i = 0:E:n
end


% function retVal = newRap(f,fp,x0,E)
% 
%     prev = x0;
%     error = 1;
%     
%     while error > E
%         % vars
%         fX0 = f(prev);
%         fpX0 = fp(prev);
%         % formula
%         act = prev - (fX0/fpX0);
%         % parada
%         error = abs(act - prev);
%         % conservar el anterior
%         prev = act;
%     end
%     
%     retVal = prev;
% end

function retVal = newRap(f,df,x0,E)
    v = symvar(f);
    prev = x0;
    loop = 1;
    while loop
        act = prev - (subs(f,v,prev)) / (subs(df,v,prev));
        if abs(prev-act) > E
            prev = act;
        else
            loop = 0;
        end
    end
    retVal = prev;
end
